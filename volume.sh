#!/bin/bash
if [[ $1 = "create" ]]
then
openstack volume create --size 1 openstack-test
elif [[ $1 = "show" ]]
then
openstack volume show openstack-test

elif [[ $1 = "delete" ]]
then
openstack volume delete openstack-test
else
echo "Error, use volume.sh create or volume.sh show or volume.sh delete"
exit 1
fi
