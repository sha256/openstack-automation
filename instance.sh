#!/bin/bash
image=
flavor=
network=
if [[ $1 = "create" ]]
then
openstack server create --image $image --flavor $flavor --network $network openstack-test
elif [[ $1 = "show" ]]
then
openstack server show openstack-test

elif [[ $1 = "delete" ]]
then
openstack server delete openstack-test
else
echo "Error, use instance.sh create or instance.sh show or instance.sh delete"
exit 1
fi
