### Read Me ###
### Variables to change ###

### auto.sh ###
#PATH - Run this command 'env | grep PATH' on the server you are executing the script from with the user you are also executing from and you will see output like below
Example 1
PATH=/home/user/bin:/home/user/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
Example 2
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/snap/bin:/var/lib/snapd/snap/bin:/snap/bin:/var/lib/snapd/snap/bin
Copy these values into PATH variable in auto.sh otherwise cronjob won't work.. remember to find the PATH values for your server and user and don't copy mine in this example
#rcfile
#company
#webhookurl
#slackchannel
#homepath - Full path where OpenStack Automation scripts are stored

### instance.sh ###
#image
#flavor
#network

### menu.sh ###
#rcfile
#homepath - Full path where OpenStack Automation scripts are stored
