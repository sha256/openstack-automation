#!/bin/bash
if [[ $1 = "create" ]]
then
openstack network create openstack-test
elif [[ $1 = "show" ]]
then
openstack network show openstack-test

elif [[ $1 = "delete" ]]
then
openstack network delete openstack-test
else
echo "Error, use network.sh create or network.sh show or network.sh delete"
exit 1
fi
