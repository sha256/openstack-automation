#!/bin/bash

rcfile=
homepath=$(pwd)
source $rcfile

PS3='Please enter your choice: '
options=("Create Volume" "Show Volume" "Delete Volume" "Create Network" "Show Network" "Delete Network" "Create Instance" "Show Instance" "Delete Instance" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Create Volume")
            echo "Creating Volume"
                        $homepath/volume.sh create
            ;;
        "Show Volume")
            echo "Showing Volume"
                        $homepath/volume.sh show
            ;;
        "Delete Volume")
            echo "Deleting Volume"
                        $homepath/volume.sh delete
            ;;
        "Create Network")
            echo "Creating Network"
                        $homepath/network.sh create
           ;;
        "Show Network")
            echo "Showing Network"
                        $homepath/network.sh show
           ;;
        "Delete Network")
            echo "Deleting Network"
                        $homepath/network.sh delete
           ;;
        "Create Instance")
            echo "Creating Instance"
                        $homepath/instance.sh create
           ;;
        "Show Instance")
            echo "Showing Instance"
                        $homepath/instance.sh show
           ;;
        "Delete Instance")
            echo "Deleting Instance"
                        $homepath/instance.sh delete
           ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
