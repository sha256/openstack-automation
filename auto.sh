#!/bin/bash
export PATH=
rcfile=
check=0
company=""
slackchannel=
homepath=
### use webhook.site or slack ###
webhookurl=""
if [[ $webhookurl == *"webhook.site"* ]]; then
webhook=webhook
elif [[ $webhookurl == *"slack.com"* ]]; then
webhook=slack
else
echo "Use Slack or Webhooks.site for webhook url"
exit -1
fi
source $rcfile
testvolume() {
     if  $homepath/volume.sh create | grep -q creating; then
          echo "Creating Volume"
          ((check++))
          sleep 15
          if  $homepath/volume.sh show | grep -q available; then
               echo "Volume Built Successfully"
               ((check++))
          $homepath/volume.sh delete
          sleep 15
          if  $homepath/volume.sh show 2>&1 | grep -q "No volume with a name or ID of 'openstack-test' exists."; then
               echo "Volume Deleted Successfully"
               ((check++))
              else
               msg_web="$company - Deleting Volume Failed: Error Code: 3 - Completed $check out of 9 checks"
               echo $msg_web
               if [[ $webhook == "webhook" ]]; then
               curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
             elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
             else
echo "Error 1111"
             fi
               #statements
               exit -1
          fi

          else
               msg_web="$company - Creating Volume Failed: Error Code: 2 - Completed $check out of 9 checks"
               echo $msg_web
               if [[ $webhook == "webhook" ]]; then
               curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
             elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
             else
echo "Error 1112"
             fi
               exit -1
          fi

     else
msg_web="$company - Creating Volume Failed: Error Code: 1 - Completed $check out of 9 checks"
echo $msg_web
if [[ $webhook == "webhook" ]]; then
curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
else
echo "Error 1113"
fi
          exit -1
     fi
 }

 testnetwork() {
   if  $homepath/network.sh create | grep -q ACTIVE; then
        echo "Created Network"
        ((check++))
        sleep 15
        if  $homepath/network.sh show | grep -q ACTIVE; then
             echo "Double Check Network"
             ((check++))
        $homepath/network.sh delete
        sleep 15
        if  $homepath/network.sh show 2>&1 | grep -q "No Network found for openstack-test"; then
             echo "Network Deleted Successfully"
             ((check++))


            else
             msg_web="$company - Deleting Network Failed: Error Code: 6 - Completed $check out of 9 checks"
             echo $msg_web
             if [[ $webhook == "webhook" ]]; then
             curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
           elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
           else
echo "Error 1114"
           fi
             exit -1
        fi

        else
             msg_web="$company - Creating Network Failed: Error Code: 5 - Completed $check out of 9 checks"
             echo $msg_web
             if [[ $webhook == "webhook" ]]; then
             curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
           elif [[ $webhook == "slack" ]]; then
 curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
           else
 echo "Error 1115"
           fi
             exit -1
        fi

   else
  msg_web="$company - Creating Network Failed: Error Code: 4 - Completed $check out of 9 checks"
  echo $msg_web
  if [[ $webhook == "webhook" ]]; then
  curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
else
echo "Error 1116"
fi
        exit -1
   fi


 }

 testinstance() {

   if  $homepath/instance.sh create | grep -q BUILD; then
        echo "Building Instance"
        ((check++))
        sleep 15
        until [ "$($homepath/instance.sh show | grep status | awk -F '|' {'print $3'} | tr -d '[:space:]')" == "ACTIVE" ] || [ "$($homepath/instance.sh show | grep status | awk -F '|' {'print $3'} | tr -d '[:space:]')" == "ERROR" ]
        do
        echo "Building Instance, please wait..."
        if  $homepath/instance.sh show 2>&1 | grep -q "More than one server exists with the name 'openstack-test'"; then
          msg_web="$company - Creating Instance Failed with error: More than one server exists with the name 'openstack-test' - Delete instance and try again.."
          echo $msg_web
          if [[ $webhook == "webhook" ]]; then
          curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
        elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
        else
echo "Error 1117"
        fi
          exit -1
      fi
        sleep 30;
      done
        if  $homepath/instance.sh show | grep -q ACTIVE; then
             echo "Instance Build Successfull"
             ((check++))
        $homepath/instance.sh delete
        sleep 15


if  $homepath/instance.sh show 2>&1 | grep -q "No server with a name or ID of 'openstack-test' exists."; then
             echo "$company - Instance Deleted Successfully"
             ((check++))


            else
             msg_web="$company - Deleting Instance Failed: Error Code: 9 - Completed $check out of 9 checks"
             echo $msg_web
             if [[ $webhook == "webhook" ]]; then
             curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
           elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
           else
echo "Error 1118"
           fi
             exit -1
        fi

        else
             msg_web="$company - Creating Instance Failed: Error Code: 8 - Completed $check out of 9 checks"
             echo $msg_web
             if [[ $webhook == "webhook" ]]; then
             curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
           elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
           else
echo "Error 1119"
           fi
             exit -1
        fi

   else
   msg_web="$company - Creating Instance Failed: Error Code: 7 - Completed $check out of 9 checks"
   echo $msg_web
   if [[ $webhook == "webhook" ]]; then
   curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
 elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
 else
echo "Error 1120"
 fi
   exit -1
   fi


 }



 testvolume
 testnetwork
 testinstance

 msg_web="$company - All Testing Finished Successfully"
 if [[ $webhook == "webhook" ]]; then
 curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_web}" $webhookurl
elif [[ $webhook == "slack" ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$msg_web.\", \"icon_emoji\": \":ghost:\"}" $webhookurl
else
echo "Error 1121"
fi
      exit -1
